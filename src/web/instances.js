fetch ('https://api.whotofollow.tk/peers.json')
	.then (response => response.json ())
	.then (data => {
		document.getElementById ('placeholder').innerHTML = ''
		for (let cn = 0; cn < data.length; cn ++) {
			instance = data.at (cn)
			let p = document.createElement ('p')

			if (instance.blocked) {
				let t = document.createTextNode (instance.hostName + ' ' + instance.softwareName + ', ' + instance.numberOfMonthlyActiveUsers + ' MAU')
				p.appendChild (t)
			} else {
				let a = document.createElement ('a')
				a.setAttribute ('target', '_blank')
				a.setAttribute ('href', 'https://' + instance.hostName)
				a.innerText = instance.hostName
				p.appendChild (a)
				let t = document.createTextNode (' ' + instance.softwareName + ', ' + instance.numberOfMonthlyActiveUsers + ' MAU')
				p.appendChild (t)
			}
			
			document.getElementById ('placeholder').appendChild (p)
		}
	})
