fetch ('https://api.whotofollow.tk/users.json')
	.then (response => response.json ())
	.then (data => {
		document.getElementById ('placeholder').innerHTML = ''
		for (let cn = 0; cn < data.length && cn < 1000; cn ++) {
			user = data.at (cn)
			if (! user.locked) {
				let p = document.createElement ('p')

				let a1 = document.createElement ('a')
				a1.setAttribute ('target', '_blank')
				a1.setAttribute ('href', 'https://' + user.hostName + '/users/' + user.userName)
				a1.innerText = user.userName + '@' + user.hostName
				p.appendChild (a1)
				let t1 = document.createTextNode (' (' + user.softwareName + ', ')
				p.appendChild (t1)
				if (0 < user.score) {
					let a2 = document.createElement ('a')
					a2.setAttribute ('target', '_blank')
					a2.setAttribute ('href', user.postUri)
					a2.innerText = user.score.toFixed ()
					p.appendChild (a2)
				} else {
					let t3 = document.createTextNode (user.score.toFixed ())
					p.appendChild (t3)
				}
				let t2 = document.createTextNode (') ' + user.screenName)
				p.appendChild (t2)

				document.getElementById ('placeholder').appendChild (p)
			}
		}
	})


