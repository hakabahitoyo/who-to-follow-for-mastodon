#ifndef WHO_TO_FOLLOW_H
#define WHO_TO_FOLLOW_H


#include <string>
#include <tuple>


namespace {


using namespace std;


class Host {
public:
	string host_name;
	string software_name;
	uint64_t number_of_monthly_active_users;
};


class User {
public:
	string host_name;
	string software_name;
	string user_name;
	string screen_name;
	bool discoverable;
	uint64_t number_of_followers;
	string avatar;
	string header;
	bool locked;
	double score;
	time_t time;
	string post_uri;
	set <string> known_post_uris;
};


class HostNameAndUserName {
public:
	string host_name;
	string user_name;
public:
	bool operator < (const HostNameAndUserName r) const {
		return tuple {host_name, user_name} < tuple {r.host_name, r.user_name};
	}
};


struct GetPeersBag {
	string url;
};


struct GetNodeinfoBag {
	string url;
	Host *host;
};


}; /* namespace { */


#endif /* #ifndef WHO_TO_FOLLOW_H */


