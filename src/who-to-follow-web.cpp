#include <map>
#include <algorithm>
#include <vector>
#include <ctime>
#include <iostream>
#include <cstdlib>
#include <deque>
#include <sstream>
#include <set>
#include <cstdio>
#include <cstdlib>
#include <tuple>

#include <curl/curl.h>

#include "mongoose.h"


using namespace std;


/* Global */


static struct mg_mgr mongoose_manager;


/* Util */


static string make_string (struct mg_str in)
{
	return string {in.ptr, in.len};
}


static bool starts_with (string x, string y)
{
	return
		y.size () <= x.size ()
		&& x.substr (0, y.size ()) == y;
}


static bool ends_with (string x, string y)
{
	return
		y.size () <= x.size ()
		&& x.substr (x.size () - y.size ()) == y;
}


static string escape_json (string in)
{
	string out;
	for (auto c: in) {
		if (c == '\n') {
			out += string {"\\n"};
		} else if (c == '\r') {
			out += string {"\\r"};
		} else if (c == '\t') {
			out += string {"\\t"};
		} else if (c == '"') {
			out += string {"\\\""};
		} else if (c == '\\') {
			out += string {"\\\\"};
		} else if (0x00 <= c && c < 0x20) {
			out += string {"�"};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


/* Server */


static string construct_404_page ()
{
	string html = string {
		"<!DOCTYPE html>\r\n"
		"<meta name=\"viewport\" content=\"width=device-width\">\r\n"
		"<title>404</title>\r\n"
		"<p>404</p>\r\n"
	};

	return html;
}


static void cb (struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
	if (ev == MG_EV_HTTP_MSG) {
		auto http_message = reinterpret_cast <struct mg_http_message *> (ev_data);
		if (make_string (http_message->method) == string {"GET"}) {
			if (make_string (http_message->uri) == string {"/"}) {
				mg_http_reply (
					c,
					200,
					"Content-Type: text/html\r\n",
					"%s",
					"<meta http-equiv=\"refresh\" content=\"0; URL=/web\">"
				);
			} else if (starts_with (make_string (http_message->uri), string {"/web"})) {
				struct mg_http_serve_opts opts {
					.root_dir = ".",
				};
				mg_http_serve_dir (c, http_message, & opts);
			} else {
				mg_http_reply (
					c,
					404,
					"Content-Type: text/html\r\n",
					"%s",
					construct_404_page ().c_str ()
				);
			}
		}
	}
}


/* Main */


int main (int argc, char *argv []) {
	mg_mgr_init (& mongoose_manager);
	mg_http_listen (& mongoose_manager, "http://localhost:8035", cb, nullptr);
	for (; ; ) {
		mg_mgr_poll (& mongoose_manager, 10);
	}
	mg_mgr_free (& mongoose_manager);
	return 0;
}

