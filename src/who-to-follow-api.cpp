#include <map>
#include <algorithm>
#include <vector>
#include <ctime>
#include <iostream>
#include <cstdlib>
#include <deque>
#include <sstream>
#include <set>
#include <cstdio>
#include <cstdlib>
#include <tuple>
#include <ctime>

#include <curl/curl.h>

#include "mongoose.h"
#include "picojson.h"

#include "who-to-follow.h"


using namespace std;


/* Global */


static struct mg_mgr mongoose_manager;
static vector <Host> known_hosts;
static vector <Host> live_hosts;
static deque <User> known_users;
static uint64_t anti_celebrity_cutoff = 250;
static set <string> blocked_host_names;
static uint64_t get_users_task_timeout = 20;
static uint64_t get_discoverable_task_timeout = 20;
static double duration_in_day = 7;
static set <HostNameAndUserName> blocked_users;
static set <string> deep_blocked_host_names;

static const uint64_t mongoose_timeout_ms = 10000;


/* Util */


static string make_string (struct mg_str in)
{
	return string {in.ptr, in.len};
}


static bool starts_with (string x, string y)
{
	return
		y.size () <= x.size ()
		&& x.substr (0, y.size ()) == y;
}


static bool ends_with (string x, string y)
{
	return
		y.size () <= x.size ()
		&& x.substr (x.size () - y.size ()) == y;
}


static string escape_json (string in)
{
	string out;
	for (auto c: in) {
		if (c == '\n') {
			out += string {"\\n"};
		} else if (c == '\r') {
			out += string {"\\r"};
		} else if (c == '\t') {
			out += string {"\\t"};
		} else if (c == '"') {
			out += string {"\\\""};
		} else if (c == '\\') {
			out += string {"\\\\"};
		} else if (0x00 <= c && c < 0x20) {
			out += string {"�"};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


/* Server */


static string construct_404_page ()
{
	string html = string {
		"<!DOCTYPE html>\r\n"
		"<meta name=\"viewport\" content=\"width=device-width\">\r\n"
		"<title>404</title>\r\n"
		"<p>404</p>\r\n"
	};

	return html;
}


static bool blocked (string host_name)
{
	return blocked_host_names.find (host_name) != blocked_host_names.end ();
}


static bool blocked (string host_name, string user_name)
{
	HostNameAndUserName user {
		.host_name = host_name,
		.user_name = user_name,
	};
	return blocked_users.find (user) != blocked_users.end ();
}


static bool by_mau (const Host a, const Host b)
{
	return
		tuple {
			b.number_of_monthly_active_users,
			a.software_name,
			a.host_name
		} < tuple {
			a.number_of_monthly_active_users,
			b.software_name,
			b.host_name
		};
}


static string dump_hosts () {
	vector <Host> sorted_hosts {live_hosts};
	stable_sort (sorted_hosts.begin (), sorted_hosts.end (), by_mau);

	string out;
	out += string {"["};
	for (auto i = sorted_hosts.begin (); i != sorted_hosts.end (); i ++) {
		if (i != sorted_hosts.begin ()) {
			out += string {","};
		}

		stringstream mau_stream;
		mau_stream << i->number_of_monthly_active_users;
		
		out += string {"\n"};
		out += string {"{"}
			+ string {"\"hostName\":\""} + escape_json (i->host_name) + string {"\","}
			+ string {"\"softwareName\":\""} + escape_json (i->software_name) + string {"\","}
			+ string {"\"numberOfMonthlyActiveUsers\":"} + mau_stream.str () + string {","}
			+ string {"\"blocked\":"} + string {blocked (i->host_name)? "true": "false"}
			+ string {"}"};
	}
	out += string {"\n]\n"};
	return out;
}


static bool by_score (const User &a, const User &b) {
	return tuple {b.score, b.time} < tuple {a.score, a.time};
};


static string dump_users () {
	vector <User> sorted_users;
	for (auto i: known_users) {
		sorted_users.push_back (i);
	}
	stable_sort (sorted_users.begin (), sorted_users.end (), by_score);

	vector <User> recommended_users;

	for (auto user: sorted_users) {
		if (2000 <= recommended_users.size ()) {
			break;
		}
		if (
			user.discoverable
			&& user.number_of_followers < anti_celebrity_cutoff
			&& (! blocked (user.host_name))
			&& (! blocked (user.host_name, user.user_name))
		) {
			recommended_users.push_back (user);
		}
	}

	string out;
	out += string {"["};
	for (auto i = recommended_users.begin (); i != recommended_users.end (); i ++) {
		stringstream score_stream;
		score_stream << i->score;
	
		stringstream time_stream;
		time_stream << i->time;
	
		if (i != recommended_users.begin ()) {
			out += string {","};
		}
		out += string {"\n"};
		out += string {"{"}
			+ string {"\"hostName\":\""} + escape_json (i->host_name) + string {"\","}
			+ string {"\"softwareName\":\""} + escape_json (i->software_name) + string {"\","}
			+ string {"\"userName\":\""} + escape_json (i->user_name) + string {"\","}
			+ string {"\"screenName\":\""} + escape_json (i->screen_name) + string {"\","}
			+ string {"\"discoverable\":"} + string {i->discoverable? "true": "false"} + string {","}
			+ string {"\"avatar\":\""} + escape_json (i->avatar) + string {"\","}
			+ string {"\"header\":\""} + escape_json (i->header) + string {"\","}
			+ string {"\"locked\":"} + string {i->locked? "true": "false"} + string  {","}
			+ string {"\"score\":"} + score_stream.str () + string {","}
			+ string {"\"time\":\""} + escape_json (time_stream.str ()) + string ("\",")
			+ string {"\"postUri\":\""} + escape_json (i->post_uri) + string ("\"")
			+ string {"}"};
	}
	out += string {"\n]\n"};
	return out;
}


static void cb (struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
	if (ev == MG_EV_HTTP_MSG) {
		auto http_message = reinterpret_cast <struct mg_http_message *> (ev_data);
		if (make_string (http_message->method) == string {"GET"}) {
			if (make_string (http_message->uri) == string {"/peers.json"}) {
				mg_http_reply (
					c,
					200,
					"Access-Control-Allow-Origin: *\r\nContent-Type: application/json\r\n",
					"%s",
					dump_hosts ().c_str ()
				);
			} else if (make_string (http_message->uri) == string {"/users.json"}) {
				mg_http_reply (
					c,
					200,
					"Access-Control-Allow-Origin: *\r\nContent-Type: application/json\r\n",
					"%s",
					dump_users ().c_str ()
				);
			} else {
				mg_http_reply (
					c,
					404,
					"Content-Type: text/html\r\n",
					"%s",
					construct_404_page ().c_str ()
				);
			}
		}
	}
}


/* Task */


static bool invalid_host_name (string host_name)
{
	return host_name.find (string {"@"}) != string::npos;
}


static bool is_deep_blocked_host (string host_name)
{
	for (string pattern: deep_blocked_host_names) {
		if (ends_with (host_name, pattern)) {
			return true;
		}
	};
	return false;
}


static void get_peers_impl (string peers_reply)
{
	picojson::value peers_value;
	try {
		string error = picojson::parse (peers_value, peers_reply);
		if (! error.empty ()) {
			cerr << error << endl;
			throw (exception {});
		}
		auto peers_array = peers_value.get <picojson::array> ();
		for (auto peer_value: peers_array) {
			string peer_string = peer_value.get <string> ();
			
			if ((! invalid_host_name (peer_string)) && (! is_deep_blocked_host (peer_string))) {
				bool hit = false;
				for (auto host: known_hosts) {
					if (peer_string == host.host_name) {
						hit = true;
						break;
					}
				}
				if (! hit) {
					Host host {
						.host_name = peer_string,
					};
					known_hosts.push_back (host);
				}
			}
		}
	} catch (exception &) {
		/* Do nothing. */
	}
}


static void get_peers_callback (struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
	GetPeersBag * get_peers_bag = reinterpret_cast <GetPeersBag *> (fn_data);

	const char *url = get_peers_bag->url.c_str ();

	if (ev == MG_EV_OPEN) {
		(* reinterpret_cast <uint64_t *> (c->data)) = mg_millis () + mongoose_timeout_ms;
	} else if (ev == MG_EV_POLL) {
		if (
			mg_millis() > (* reinterpret_cast <uint64_t *> (c->data))
			&& (c->is_connecting || c->is_resolving)
		) {
			mg_error (c, "Connect timeout");
		}
	}
	else if (ev == MG_EV_CONNECT) {
		struct mg_str host = mg_url_host (url);
		if (mg_url_is_ssl (url)) {
			struct mg_tls_opts opts = {
				.ca = "cacert.pem",
				.srvname = host,
			};
			mg_tls_init (c, &opts);
		}
		mg_printf (
			c,
			"GET %s HTTP/1.0\r\nHost: %.*s\r\n\r\n",
			mg_url_uri (url),
			(int) host.len,
			host.ptr
		);
	} else if (ev == MG_EV_HTTP_MSG) {
		auto http_message = reinterpret_cast <struct mg_http_message *> (ev_data);

		get_peers_impl (make_string (http_message->body));

		delete get_peers_bag;
		c->is_closing = 1;
	}
}


static void get_peers_task (string host_name)
{
	cerr << "[[get_peers_task]] " << host_name << endl;

	string url = string {"https://"} + host_name + string {"/api/v1/instance/peers"};

	auto get_peers_bag = new GetPeersBag {
		.url = url,
	}; /* Delete in callback. */
	
	mg_http_connect (& mongoose_manager, url.c_str (), get_peers_callback, get_peers_bag);
}


static void get_nodeinfo_callback_2 (struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
	GetNodeinfoBag * get_nodeinfo_bag = reinterpret_cast <GetNodeinfoBag *> (fn_data);

	const char *url = get_nodeinfo_bag->url.c_str ();

	if (ev == MG_EV_OPEN) {
		(* reinterpret_cast <uint64_t *> (c->data)) = mg_millis () + mongoose_timeout_ms;
	} else if (ev == MG_EV_POLL) {
		if (
			mg_millis() > (* reinterpret_cast <uint64_t *> (c->data))
			&& (c->is_connecting || c->is_resolving)
		) {
			mg_error (c, "Connect timeout");
		}
	} else if (ev == MG_EV_CONNECT) {
		struct mg_str host = mg_url_host (url);
		if (mg_url_is_ssl (url)) {
			struct mg_tls_opts opts = {
				.ca = "cacert.pem",
				.srvname = host,
			};
			mg_tls_init (c, &opts);
		}
		mg_printf (
			c,
			"GET %s HTTP/1.0\r\nHost: %.*s\r\n\r\n",
			mg_url_uri (url),
			(int) host.len,
			host.ptr
		);
	} else if (ev == MG_EV_HTTP_MSG) {
		auto http_message = reinterpret_cast <struct mg_http_message *> (ev_data);
		try {
			picojson::value nodeinfo_value;
			string error = picojson::parse (nodeinfo_value, make_string (http_message->body));
			if (! error.empty ()) {
				cerr << error << endl;
				throw (exception {});
			}
			auto nodeinfo_object = nodeinfo_value.get <picojson::object> ();
			auto software_object = nodeinfo_object.at (string {"software"}).get <picojson::object> ();
			string software_name = software_object.at (string {"name"}).get <string> ();
			cerr << "[[software_name]]" << software_name << endl;
			get_nodeinfo_bag->host->software_name = software_name;
			
			get_nodeinfo_bag->host->number_of_monthly_active_users = 0;
			try {
				auto usage_object = nodeinfo_object.at (string {"usage"}).get <picojson::object> ();
				auto users_object = usage_object.at (string {"users"}).get <picojson::object> ();
				double active_month_double = users_object.at (string {"activeMonth"}).get <double> ();
				get_nodeinfo_bag->host->number_of_monthly_active_users
					= static_cast <uint64_t> (active_month_double);
			} catch (exception &) {
				cerr << "[[usage.users.activeMonth not implemented]] " << get_nodeinfo_bag->host->host_name << endl;
			}
		} catch (exception &) {
			/* Do nothing. */
		}
		
		delete get_nodeinfo_bag;
		c->is_closing = 1;
	}
}


static void get_nodeinfo_callback_1 (struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
	GetNodeinfoBag * get_nodeinfo_bag = reinterpret_cast <GetNodeinfoBag *> (fn_data);

	const char *url = get_nodeinfo_bag->url.c_str ();

	if (ev == MG_EV_OPEN) {
		(* reinterpret_cast <uint64_t *> (c->data)) = mg_millis () + mongoose_timeout_ms;
	} else if (ev == MG_EV_POLL) {
		if (
			mg_millis() > (* reinterpret_cast <uint64_t *> (c->data))
			&& (c->is_connecting || c->is_resolving)
		) {
			mg_error (c, "Connect timeout");
		}
	} else if (ev == MG_EV_CONNECT) {
		struct mg_str host = mg_url_host (url);
		if (mg_url_is_ssl (url)) {
			struct mg_tls_opts opts = {
				.ca = "cacert.pem",
				.srvname = host,
			};
			mg_tls_init (c, &opts);
		}
		mg_printf (
			c,
			"GET %s HTTP/1.0\r\nHost: %.*s\r\n\r\n",
			mg_url_uri (url),
			(int) host.len,
			host.ptr
		);
	} else if (ev == MG_EV_HTTP_MSG) {
		auto http_message = reinterpret_cast <struct mg_http_message *> (ev_data);

		try {
			picojson::value well_known_value;
			string error = picojson::parse (well_known_value, make_string (http_message->body));
			if (! error.empty ()) {
				cerr << error << endl;
				throw (exception {});
			}
			auto well_known_object = well_known_value.get <picojson::object> ();
			auto links_value = well_known_object.at (string {"links"});
			auto links_array = links_value.get <picojson::array> ();
			auto link_value = links_array.back ();
			auto link_object = link_value.get <picojson::object> ();
			string href_string = link_object.at (string {"href"}).get <string> ();
			
			get_nodeinfo_bag->url = href_string;
			cerr << "[[nodeinfo url]]" << href_string << endl;
			mg_http_connect (& mongoose_manager, href_string.c_str (), get_nodeinfo_callback_2, get_nodeinfo_bag);
			
		} catch (exception &) {
			/* Do nothing. */
		}
		c->is_closing = 1;
	}
}


static void get_nodeinfo_task (Host &host)
{
	cerr << "[[get_nodeinfo_task]] " << host.host_name << endl;

	string url = string {"https://"} + host.host_name + string {"/.well-known/nodeinfo"};

	auto get_nodeinfo_bag = new GetNodeinfoBag {
		.url = url,
		.host = & host,
	}; /* Delete in callback. */
	
	mg_http_connect (& mongoose_manager, url.c_str (), get_nodeinfo_callback_1, get_nodeinfo_bag);
}


static size_t curl_string_callback (char *ptr, size_t size, size_t nmemb, void *userdata)
{
	string *body = reinterpret_cast <string *> (userdata);
	for (size_t cn = 0; cn < size * nmemb; cn ++) {
		body->push_back (static_cast <char> (ptr [cn]));
	}
	return size * nmemb;
}


static bool get_discoverable (CURL *curl, string host_name, string user_name)
{
	cerr << "[[get_discoverable]] " + host_name + " " + user_name << endl;

	bool discoverable = false;
	try {
		string url = string {"https://"} + host_name + string {"/users/"} + user_name;
		string body;
	
		struct curl_slist *chunk = nullptr;
		chunk = curl_slist_append (chunk, "Accept: application/activity+json");
		
		curl_easy_reset (curl);
		curl_easy_setopt (curl, CURLOPT_HTTPHEADER, chunk);
		curl_easy_setopt (curl, CURLOPT_URL, url.c_str ());
		curl_easy_setopt (curl, CURLOPT_WRITEFUNCTION, curl_string_callback);
		curl_easy_setopt (curl, CURLOPT_WRITEDATA, & body);
		curl_easy_setopt (curl, CURLOPT_TIMEOUT, get_discoverable_task_timeout);
       		CURLcode res = curl_easy_perform (curl);
		if (res != CURLE_OK) {
			cerr << curl_easy_strerror (res) << endl;
			throw (exception {});
		}

		picojson::value user_value;
		string error = picojson::parse (user_value, body);
		if (! error.empty ()) {
			cerr << error << endl;
			throw (exception {});
		}
		auto user_object = user_value.get <picojson::object> ();
		bool discoverable_bool = user_object.at (string {"discoverable"}).get <bool> ();
		discoverable = discoverable_bool;
	} catch (exception &) {
		cerr << "[[error get_discoverable]] " + host_name + " " + user_name << endl;
	}
	return discoverable;
}


static bool get_discoverable_with_cache
	(CURL *curl, string host_name, string user_name, map <string, bool> & cache)
{
	bool discoverable = false;
	if (cache.find (user_name) == cache.end ()) {
		discoverable = get_discoverable (curl, host_name, user_name);
		cache.insert (pair <string, bool> (user_name, discoverable));
	} else {
		discoverable = cache.at (user_name);
	}
	return discoverable;
}


static string to_lower (string in)
{
	string out;
	for (char c: in) {
		if ('A' <= c && c <= 'Z') {
			out.push_back (c - 'A' + 'a');
		} else {
			out.push_back (c);
		}
	}
	return out;
}


static void get_users_task (Host host)
{
	cerr << "[[get_users_task]] " << host.host_name << endl;

	CURL *curl = curl_easy_init ();

	try {
		string url = string {"https://"} + host.host_name + string {"/api/v1/timelines/public?local=true"};
		string body;

		curl_easy_reset (curl);
		curl_easy_setopt (curl, CURLOPT_URL, url.c_str ());
		curl_easy_setopt (curl, CURLOPT_WRITEFUNCTION, curl_string_callback);
		curl_easy_setopt (curl, CURLOPT_WRITEDATA, & body);
		curl_easy_setopt (curl, CURLOPT_TIMEOUT, get_users_task_timeout);
       		CURLcode res = curl_easy_perform (curl);
		if (res != CURLE_OK) {
			cerr << curl_easy_strerror (res) << endl;
			throw (exception {});
		}

		picojson::value timeline_value;
		string error = picojson::parse (timeline_value, body);
		if (! error.empty ()) {
			cerr << error << endl;
			throw (exception {});
		}
		auto timeline_array = timeline_value.get <picojson::array> ();

		map <string, bool> discoverable_cache;

		for (auto post_value: timeline_array) {
			mg_mgr_poll (& mongoose_manager, 10);
	
			auto post_object = post_value.get <picojson::object> ();
			
			auto account_object = post_object.at (string {"account"}).get <picojson::object> ();
			string username_string = account_object.at (string {"username"}).get <string> ();
			string display_name_string = account_object.at (string {"display_name"}).get <string> ();
			double followers_count_double = account_object.at (string {"followers_count"}).get <double> ();
			bool locked_bool = account_object.at (string {"locked"}).get <bool> ();
			
			string avatar;
			string header;
			try {
				avatar = account_object.at (string {"avatar_static"}).get <string> ();
				header = account_object.at (string {"header_static"}).get <string> ();
			} catch (exception &e) {
				cerr << "[[avatar or background are not implemented.]]" << endl;
			}
			
			bool discoverable = get_discoverable_with_cache
				(curl, host.host_name, username_string, discoverable_cache);
			
			string bio;
			try {
				bio = account_object.at (string {"note"}).get <string> ();
			} catch (exception &e) {
				cerr << "[[bio is not implemented.]]" << endl;
			}
			
			set <string> affirmative_action_words {
				"she/her",
				"they/them",
				"woman",
				"girl",
			};
			
			double affirmative_action_ratio = 1.0;
			
			string bio_case_insensitive = to_lower (bio);
			
			for (auto word: affirmative_action_words) {
				string word_case_insensitive = to_lower (word);
				if (bio_case_insensitive.find (word_case_insensitive) != string::npos) {
					affirmative_action_ratio = 1.5;
					break;
				}
			}
			
			double score = 0;
			string post_uri;
			try {
				double reblogs_count = post_object.at (string {"reblogs_count"}).get <double> ();
				double favourites_count = post_object.at (string {"favourites_count"}).get <double> ();
				score = (reblogs_count * 2 + favourites_count) * affirmative_action_ratio;
				post_uri = post_object.at (string {"uri"}).get <string> ();
			} catch (exception &e) {
				cerr << "[[reblogs_count or favourites_count are not implemented.]]" << endl;
			}
			
			bool hit = false;
			for (auto & known_user: known_users) {
				if (known_user.host_name == host.host_name && known_user.user_name == username_string) {
					hit = true;
					known_user.software_name = host.software_name;
					known_user.screen_name = display_name_string;
					known_user.discoverable = discoverable;
					known_user.number_of_followers = static_cast <uint64_t> (followers_count_double);
					known_user.avatar = avatar;
					known_user.header = header;
					known_user.locked = locked_bool;
					if (known_user.score <= score
						&& known_user.known_post_uris.find (post_uri) == known_user.known_post_uris.end ()
					) {
						known_user.score = score;
						known_user.time = time (nullptr);
						known_user.post_uri = post_uri;
						known_user.known_post_uris.insert (post_uri);
					}
				}
			}
			if ((! hit) && discoverable) {
				User new_user {
					.host_name = host.host_name,
					.software_name = host.software_name,
					.user_name = username_string,
					.screen_name = display_name_string,
					.discoverable = true,
					.number_of_followers = static_cast <uint64_t> (followers_count_double),
					.avatar = avatar,
					.header = header,
					.locked = locked_bool,
					.score = score,
					.time = time (nullptr),
					.post_uri = post_uri,
					.known_post_uris = set <string> {post_uri},
				};
				known_users.push_front (new_user);
			}
		}
	} catch (exception &) {
		cerr << "[[error get_users_task]] " << host.host_name << endl;
	}

	curl_easy_cleanup (curl);
}


static void load_preferences_task ()
{
	anti_celebrity_cutoff = 250;
	get_users_task_timeout = 20;
	get_discoverable_task_timeout = 20;
	duration_in_day = 7;
	
	blocked_host_names.clear ();
	blocked_users.clear ();
	deep_blocked_host_names.clear ();

	cerr << "[[PREFERENCES: load preferences task.]]" << endl;

	try {
		FILE *in = fopen ("preferences.json", "r");
		if (in == nullptr) {
			cerr << "[[PREFERENCES: No preferences.json.]]" << endl;
			throw (exception {});
		}
		string s;
		char buffer [4096];
		for (; ; ) {
			if (feof (in)) {
				break;
			}
			fgets (buffer, 4096, in);
			s += buffer;
		}
		fclose (in);
		
		picojson::value json_value;
		string json_error = picojson::parse (json_value, s);
		if (! json_error.empty ()) {
			cerr << json_error << endl;
			throw (exception {});
		}
		auto json_object = json_value.get <picojson::object> ();
		
		double anti_celebrity_cutoff_double = json_object.at (string {"antiCelebrityCutoff"}).get <double> ();
		anti_celebrity_cutoff = static_cast <uint64_t> (anti_celebrity_cutoff_double);
		
		double get_users_task_timeout_double = json_object.at (string {"getUsersTaskTimeout"}).get <double> ();
		get_users_task_timeout = static_cast <uint64_t> (get_users_task_timeout_double);
		
		double get_discoverable_task_timeout_double = json_object.at (string {"getDiscoverableTaskTimeout"}).get <double> ();
		get_discoverable_task_timeout = static_cast <uint64_t> (get_discoverable_task_timeout_double);
		
		duration_in_day = json_object.at (string {"duration"}).get <double> ();
		
		auto blocked_hosts_array = json_object.at (string {"blockedHosts"}).get <picojson::array> ();
		for (auto host_name_value: blocked_hosts_array) {
			string host_name_string = host_name_value.get <string> ();
			blocked_host_names.insert (host_name_string);
		}
		
		auto blocked_users_array = json_object.at (string {"blockedUsers"}).get <picojson::array> ();
		for (auto user_value: blocked_users_array) {
			auto user_object = user_value.get <picojson::object> ();
			string host_name = user_object.at (string {"host"}).get <string> ();
			string user_name = user_object.at (string {"user"}).get <string> ();
			HostNameAndUserName user {
				.host_name = host_name,
				.user_name = user_name,
			};
			blocked_users.insert (user);
		}
		
		auto deep_blocked_hosts_array = json_object.at (string {"deepBlockedHosts"}).get <picojson::array> ();
		for (auto deep_host_name_value: deep_blocked_hosts_array) {
			string deep_host_name_string = deep_host_name_value.get <string> ();
			deep_blocked_host_names.insert (deep_host_name_string);
		}

	} catch (exception &) {
		cerr << "[[PREFERENCES: error]]" << endl;
	}

	cerr << "[[PREFERENCES: anti_celebrity_cutoff: " << anti_celebrity_cutoff << "]]" << endl;
	cerr << "[[PREFERENCES: get_users_task_timeout: " << get_users_task_timeout << "]]" << endl;
	cerr << "[[PREFERENCES: get_discoverable_task_timeout: " << get_discoverable_task_timeout << "]]" << endl;
	cerr << "[[PREFERENCES: duration: " << duration_in_day << "]]" << endl;

	for (auto host_name: blocked_host_names) {
		cerr << "[[PREFERENCES: blocked_host_name: " << host_name << "]]" << endl;
	}
	
	for (auto user: blocked_users) {
		cerr << "[[PREFERENCES: blocked_users: " << user.host_name << " " << user.user_name << "]]" << endl;
	};

	for (auto host_name: deep_blocked_host_names) {
		cerr << "[[PREFERENCES: deep_blocked_host_name: " << host_name << "]]" << endl;
	}
}


static void deep_blocked_host_clearance_task ()
{
	cerr << "[[deep_blocked_host_clearance_task]]" << endl;
	vector <Host> new_known_hosts;
	for (auto host: known_hosts) {
		if (! is_deep_blocked_host (host.host_name)) {
			new_known_hosts.push_back (host);
		}
	}
	known_hosts = new_known_hosts;
}


static void expired_score_clearance_task ()
{
	cerr << "[[deep_blocked_host_clearance_task]]" << endl;
	time_t current_time = time (nullptr);
	const time_t duration_in_sec = static_cast <time_t> (duration_in_day * 24 * 60 * 60);
	for (auto &user: known_users) {
		if (user.time + duration_in_sec < current_time) {
			user.score = 0;
			user.time = 0;
			user.post_uri = string {};
		}
	}
}


/* Main */


int main (int argc, char *argv []) {
	const set <string> root_hosts {
		string {"mastodon.social"},
		string {"mastodon.world"},
		string {"mas.to"},
		string {"mstdn.jp"},
		string {"baraag.net"},
		string {"mstdn.social"},
		string {"stereophonic.space"},
		string {"cawfee.club"},
		string {"shitposter.club"},
		string {"blob.cat"},
		string {"fedi.absturztau.be"},
		string {"cdrom.tokyo"},
	};
	
	for (auto host_name: root_hosts) {
		Host host {
			.host_name = host_name,
		};
		known_hosts.push_back (host);
	}
	
	mg_mgr_init (& mongoose_manager);
	mg_http_listen (& mongoose_manager, "http://localhost:8036", cb, nullptr);

	known_hosts.reserve (1000000);

	curl_global_init (CURL_GLOBAL_ALL);

	for (; ; ) {
		mg_mgr_poll (& mongoose_manager, 10);

		load_preferences_task ();
		
		mg_mgr_poll (& mongoose_manager, 10);

		deep_blocked_host_clearance_task ();

		expired_score_clearance_task ();

		size_t num_known_hosts = known_hosts.size ();
		for (size_t cn = 0; cn < num_known_hosts; cn ++) {
			mg_mgr_poll (& mongoose_manager, 10);
			get_nodeinfo_task (known_hosts.at (cn));
		}

		live_hosts.clear ();
		for (auto host: known_hosts) {
			if (! host.software_name.empty ()) {
				live_hosts.push_back (host);
			}
		}

		for (auto host: live_hosts) {
			mg_mgr_poll (& mongoose_manager, 10);
			get_users_task (host);
			mg_mgr_poll (& mongoose_manager, 10);
			get_peers_task (host.host_name);
		}

	}

	curl_global_cleanup ();

	mg_mgr_free (& mongoose_manager);
	return 0;
}

