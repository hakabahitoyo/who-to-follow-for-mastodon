# Who To Follow For Mastodon & Pleroma

https://whotofollow.tk

## Install

### Build

Build as follows:

```
sudo apt install libssl-dev libcurl4-openssl-dev
cd src
make clean
make
```

### Edit JavaScripts

Replace `api.whotofollow.tk` with your API host name in `{instances,users-avatar-off,users-avatar-on}.js`.

### Write Caddyfile

Write following Caddyfile:

```
whotofollow.tk {
	reverse_proxy :8035
}

api.whotofollow.tk {
	reverse_proxy :8036
}
```

Replace `whotofollow.tk` with your web host name, `api.whotofollow.tk` with your API host name respectively.

## Run

In the screen command:

```
cd src
./who-to-follow-web
```

In the other screen:

```
cd src
./who-to-follow-api
```

In the other screen:

```
sudo caddy run
```

## API

* GET https://api.whotofollow.tk/users.json
* GET https://api.whotofollow.tk/peers.json

## Thanks

* https://github.com/cesanta/mongoose
* https://curl.haxx.se/docs/caextract.html
* https://github.com/kazuho/picojson

